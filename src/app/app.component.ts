import { Component } from '@angular/core';
import { RegistrationUser } from './registration-user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  submitted = false;
  nationalities = [{desc: 'Singaporean', value: 'SG'}, {desc: 'Malaysian', value: 'MY'},
  {desc: 'British', value: 'UK'}, {desc: 'Australian', value: 'AU'}];

  model = new RegistrationUser('', '', '', '', 'M', null, '', '', '');

  onSubmit() {
    this.submitted = true;
    console.log(JSON.stringify(this.model));
  }

  onChange(evt) {

  }

  get diagnostic() { return JSON.stringify(this.model); }
}
